
--
-- Table structure for table `keyclock`
--

DROP TABLE IF EXISTS `keyclock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `keyclock` (
  `id` varchar(150) NOT NULL,
  `user_id` bigint NOT NULL,
  `login_id` varchar(255) NOT NULL,
  `valid_until` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user_id`,`login_id`),
  KEY `valid` (`valid_until`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(75) NOT NULL,
  `status` tinyint NOT NULL DEFAULT '0',
  `register_datetime` datetime NOT NULL,
  `update_datetime` varchar(45) NOT NULL,
  `register_ip` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `password` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `adty` varchar(30) NOT NULL,
  `name` varchar(75) NOT NULL,
  `address_line_1` varchar(150) NOT NULL,
  `address_line_2` varchar(150) NOT NULL,
  `address_line_3` varchar(150) DEFAULT NULL,
  `city` varchar(150) NOT NULL,
  `zip` varchar(15) NOT NULL,
  `state` varchar(10) NOT NULL,
  `country` varchar(5) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `add_datetime` datetime NOT NULL,
  `updated_dateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`adty`),
  ADD KEY `cityStateCountry` (`city`,`zip`,`state`,`country`),
  ADD KEY `address` (`address_line_1`,`address_line_2`,`address_line_3`,`city`,`zip`,`country`),
  ADD KEY `AddedBY` (`added_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
