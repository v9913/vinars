package repository

import (
	"../../lib/core/database"
	"strconv"
	"time"
)

func InsertUser (
	db *database.Database,
	email string,
	password string,
	status int,
	ipAddress string,
	) (userId int64, err error) {

	var data = map[string]string {
		"email" : email,
		"password" : password,
		"status" : strconv.Itoa(status),
		"register_ip" : ipAddress,
		"register_datetime" : time.Now().Format("2006-01-02 15:04:05"),
		"update_datetime" : time.Now().Format("2006-01-02 15:04:05"),
	}

	userId, err = db.Insert("user",  data)
	if err != nil {
		return 0, err
	}

	return userId, nil
}
func GetUserByEmail(db *database.Database,
	email string,
) (userId int64, password string, status int8, err error)  {

	var condition = map[string]string {
		"email" : email,
	}
	var fields = []string{
		"id",
		"email",
		"password",
		"status",
	}
	var orderBy = []string{"id"}
	rows, err := db.Select("user", fields , condition, orderBy, 1)
	if err != nil {
		return 0, "",  0, err
	}

	if rows.Next() {
		err := rows.Scan(&userId, &email, &password, &status)
		if err == nil {
			return userId, password, status, nil
		}
	}

	return 0, "",  0, err
}

func IsUserExistByEmail(
	db *database.Database,
	email string,
	) (bool, error)  {
	var condition = map[string]string {
		"email" : email,
	}
	var fields = []string{
		"id",
	}
	var orderBy = []string{"id"}
	rows, err := db.Select("user", fields , condition, orderBy, 1)
	if err != nil {
		return false, err
	}

	if rows.Next() {
		return true, nil
	}

	return false, nil
}