package read
import (
	"../../service"
	"fmt"
	response "../../../lib/core/response"
)
type userAddressSuccessWithData struct {
	Address []service.UserAddress
}
func (c *ReadController) UserAddressList () {
	var ua service.UserAddress
	//check permission
	//@todo

	result, err := ua.GetAllUserAddressForUser(c.DB, c.UserId)

	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}

	data := formatUserAddressResponse(result)
	c.Result = response.ResponseWithData(data, "")
	return
}

func (c *ReadController) UserAddressDetail (addressId int64) {
	var ua service.UserAddress
	//check permission
	//@todo
	ua.Id = addressId

	result, err := ua.GetUserAddressById(c.DB, addressId)

	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}

	data := formatUserAddressResponse(result)
	c.Result = response.ResponseWithData(data, "")
	return
}

func formatUserAddressResponse(result []service.UserAddress) []interface{} {
	pRes := userAddressSuccessWithData{
		Address: result,
	}
	var data []interface{}
	return append(data, pRes)
}
