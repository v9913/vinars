package write

import (
	"encoding/json"
	"../../service"
	"fmt"
	response "../../../lib/core/response"
)
type loginSuccessWithData struct {
	AuthToken string
}
func (c *WriteController) Login () {
	var u service.User
	err := json.NewDecoder(c.Request.Body).Decode(&u)
	if err != nil {
		c.Result, c.StatusCode = response.ErrorCodeResponse("INVALID_INPUT")
		return
	}
	if  c.UserId != 0 {
		c.Result, c.StatusCode = response.ErrorCodeResponse("ALREADY_LOGGED_IN")
		return
	}

	err, token := u.Login(c.DB, c.Configuration, c.DeviceType, c.DeviceToken)
	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}
	if token != "" {
		c.Result = response.ResponseWithData(formatResponseLogin(token), "Logged-in")
		return
	}
}

func (c *WriteController) SocialLogin () {

}

func (c *WriteController) MobileLogin () {

}

func (c *WriteController) ResetPassword () {
	var p service.Password
	err := json.NewDecoder(c.Request.Body).Decode(&p)
	if err != nil {
		c.Result, c.StatusCode = response.ErrorCodeResponse("INVALID_INPUT")
		return
	}
	err = p.ResetPassword(c.DB, c.Configuration)
	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}

	c.Result = response.ResponseWithoutData("SUCCESS", "Confirmation link sent to your email")
}

func (c *WriteController) ChangePassword () {
	var p service.Password
	err := json.NewDecoder(c.Request.Body).Decode(&p)
	if err != nil {
		c.Result, c.StatusCode = response.ErrorCodeResponse("INVALID_INPUT")
		return
	}
	err = p.ChangePassword(c.DB, c.Configuration, c.UserId)
	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}

	c.Result = response.ResponseWithoutData("SUCCESS", "Password updated successfully")
}

func formatResponseLogin(result string) []interface{} {
	pRes := loginSuccessWithData{
		AuthToken: result,
	}
	var data []interface{}
	return append(data, pRes)
}