package write

import (
	"encoding/json"
	"../../service"
	"fmt"
	response "../../../lib/core/response"
	"strconv"
)

func (c *WriteController) AddUserAddress () {
	var ua service.UserAddress
	//check permission
	//@todo
	err := json.NewDecoder(c.Request.Body).Decode(&ua)
	if err != nil {
		c.Result, c.StatusCode = response.ErrorCodeResponse("INVALID_INPUT")
		return
	}
	if ua.UserId == 0 {
		ua.UserId = c.UserId
	}

	ua.AddedBy = c.UserId
	err, addressId := ua.AddUserAddress(c.DB)
	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}
	if addressId != 0 {
		lRes := map[string]string{
			"addressId": strconv.FormatInt(addressId, 10),
		}
		var data []interface{}
		data = append(data, lRes)
		c.Result = response.ResponseWithData(data, "Address added")
		return
	}
}

func (c *WriteController) UpdateUserAddress (addressId int64) {
	var ua service.UserAddress
	//check permission
	//@todo
	err := json.NewDecoder(c.Request.Body).Decode(&ua)
	if err != nil {
		c.Result, c.StatusCode = response.ErrorCodeResponse("INVALID_INPUT")
		return
	}

	ua.Id = addressId
	ua.AddedBy = c.UserId
	err = ua.UpdateUserAddress(c.DB)
	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}

	c.Result = response.ResponseWithoutData("SUCCESS", "Address updated")
	return
}

func (c *WriteController) RemoveUserAddress (addressId int64) {
	var ua service.UserAddress
	//check permission
	//@todo
	ua.Id = addressId
	ua.AddedBy = c.UserId
	err := ua.DeleteUserAddress(c.DB)
	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}
	c.Result = response.ResponseWithoutData("SUCCESS", "Address removed")
	return
}

func (c *WriteController) DeactivateUserAddress (addressId int64) {
	var ua service.UserAddress
	//check permission
	//@todo
	ua.Id = addressId
	ua.AddedBy = c.UserId
	err := ua.DeactivateUserAddress(c.DB)
	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}
	c.Result = response.ResponseWithoutData("SUCCESS", "Address deactivated")
	return
}

func (c *WriteController) ActivateUserAddress (addressId int64) {
	var ua service.UserAddress
	//check permission
	//@todo
	ua.Id = addressId
	ua.AddedBy = c.UserId
	err := ua.ActivateUserAddress(c.DB)
	if err != nil {
		//log error
		fmt.Println(err)
		c.Result, c.StatusCode = response.ErrorResponse(err)
		return
	}
	c.Result = response.ResponseWithoutData("SUCCESS", "Address activated")
	return
}