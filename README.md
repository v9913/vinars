Project Name: VinArs
Version: 1.0 (Ready to go)
Date: 10 Oct 2019

@copyright: http://vinars.in 2019 under GPL, free to use as per your requirement.
 
 * For any help in base framework
 * email to: Vinay kant Sahu (vinaykantsahu@gmail.com)
 * Please update the config files according to your requirements. 
 You can find them under the "configuration" on the root of project.
 * Make sure the proper permissions for "logs" folder to save logs and 'img' for images
 
 
 